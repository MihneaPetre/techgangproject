import React from 'react';
import './Register.css';
//import axios from 'axios';
import {Login} from './../login/Login';

export class Register extends React.Component{
    constructor(props){
        super(props);
        this.state={};
        this.state.emailAdress="";
        this.state.firstName="";
        this.state.lastName="";
        this.state.userType=0;
        this.state.password="";
        this.state.codeId=0;
        this.state.action="";
    }
    

    handleChangeFirstName=(event)=>{
        this.setState({
            firstName: event.target.value
        })
    }
    handleChangeLastName=(event)=>{
        this.setState({
            lastName:event.target.value
        })
    }
    handleChangeEmail=(event)=>{
        this.setState({
            emailAdress:event.target.value
        })
    }
    handleChangePassword=(event)=>{
        this.setState({
            password:event.target.value
        })
    }
    handleClickBack=()=>{
        this.setState({
            action: "back"
        })
    }
    handleChangeOption=()=> {
        var code=document.getElementById("codeInput");
        var type=document.getElementById("userTypeSelector").value;
        if(Number(type)===1)
        {
            code.style.visibility="hidden";
            code.innerHTML=11111;
        }
        else{
            code.style.visibility="visible";
            code.innerHTML=Number(this.state.codeId.value);
        }
    }
    handleForwardClick=()=>{
        //     let user={
        //         emailAdress:this.state.emailAdress,
        //         firstName:this.state.firstName,
        //         lastName:this.state.lastName,
        //         userType:Number(this.state.userType),
        //         password:this.state.password,
        //         codeId:100
        //     }   
        // axios.post("http://localhost:3000/users/register", user).then((res)=>{
        //     if(res.status===200)
        //     {
        //         this.props.userAdded(user)
        //     }
        // }).catch((err)=>{
        //     console.log(err);
        // })
        // var today = new Date();
        // var dd = today.getDate();
        // var mm = today.getMonth() + 1;
        // var yyyy = today.getFullYear();

        fetch('http://localhost:5000/users/register2/', {
        method: 'POST',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
         },
         body: JSON.stringify({
            emailAdress:this.state.emailAdress,
            firstName:this.state.firstName,
            lastName:this.state.lastName,
            userType:Number(this.state.userType.value),
            password:this.state.password,
            codeId:Number(this.state.codeId.value)      
        })}).then((res)=>{
            this.setState({action:"login"})
            console.log(res) ;
        })
    
    }
    render()
    {
        if(this.state.action === ""){
        return(

        <div className="container-fluid">
            <div className="row justify-content-center">
                <div className="col-12 col-sm-6 col-md-3 bg-light">
                    <form className="form-container ">
                      <div className="form-group">
                          <h1>Welcome!</h1>
                          <p>Let's begin our journey</p>
                      </div>  
                      <div className="form-group">
                          <button type="button" className="btn btn-primary btn-lg btn-block">Register with Facebook</button>
                      </div>
                      <div className="form-group" id="icon1">
                          <i>Who are you?</i>
                      </div>
                      <div className="form-group">
                          <div className="row">
                              <div className="col-sm-6">
                                 <div className="form-group">
                                     <input name="nume" className="form-control " type="text" placeholder="First Name" onChange={this.handleChangeFirstName}/>
                                 </div>
                              </div>
                              <div className="col-sm-6">
                                <div className="form-group">
                                    <input name="prenume" className="form-control " type="text" placeholder="Last Name" onChange={this.handleChangeLastName} />
                                </div>
                              </div>
                          </div>
                          <div className="form-group" id="icon2">
                               <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="     Email" onChange={this.handleChangeEmail} />
                          </div>
                          <div className="form-group" id="icon3">
                               <input name="parola" type="password" className="form-control" placeholder="     Password" onChange={this.handleChangePassword}/>
                          </div>
                          <div className="form-group" id="icon3">
                               <input name="confirmaparola" type="password" className="form-control" placeholder="     Confirm Password"  onChange={this.handleChangePassword}/>
                          </div>
                          
                      </div>
                      <div className="form-group">
                          <div className="row">
                             <div className="col-sm-6">
                              <i>User type:</i>
                              </div>
                              <div className="col-sm-6">
                                 <div className="form-group">
                                    <select className="form-control" id="userTypeSelector" onChange={this.handleChangeOption}>
                                      <option value="2">Student</option>
                                      <option value="1">Profesor</option>
                                      
                                      
                                    </select>
                                 </div>
                              </div>
                            </div>
                        </div>
                      <div className="form-group">
                            <input name="Cod" type="text" className="form-control" placeholder="Code" id="codeInput"></input>
                      </div>
                      <div className="col-12 bg-light" styles={{ textAlign:'center', marginTop:'70px' }}>
                        <a href="htttp://ase.ro" >Do you have an account? Sign in</a>
                      </div> 
                      <div className="form-group" style={{marginTop:"80px"}}>
                          <div className="row">
                              <div className="col-sm-6">
                                 <div className="form-group">
                                    <button type="button" onClick={this.handleClickBack} className="btn btn-block btn-lg" style={{backgroundColor:"transparent",borderColor:"#4466a3"}}><span className="glyphicon glyphicon-menu-left" aria-hidden="true" style={{marginRight:"10px"}}></span>Back </button>
                                 </div>
                              </div>
                              <div className="col-sm-6">
                                <div className="form-group">
                                    <button type="button"  className="btn btn-block btn-lg" onClick={this.handleForwardClick} style={{backgroundColor:"transparent",borderColor:"#4466a3"}}>Forward <span className="glyphicon glyphicon-menu-right" aria-hidden="true" style={{marginRight:"10px"}}></span></button>
                                </div>
                              </div>
                              
                          </div>
                          
                      </div>
                    </form>
                </div>
            </div>
        </div> 
       
    
        )
        }else if(this.state.action === "login"){
            return(
                <Login></Login>
            )
        }
    }

}