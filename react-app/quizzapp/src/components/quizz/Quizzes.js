import React from 'react';
import './Quizzes.css';


export class Quizzes extends React.Component{
    constructor(props){
        super(props);

        this.state={
            tests: []
        };
    }

    componentWillMount(){
        fetch('http://localhost:5000/quizzes')
        .then( (results) =>{return results.json();}  )
        .then( (tests) => {this.setState({tests})})

        console.log(this.state.tests);
    }

    render()
    {
        return(
            <React.Fragment>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark shadow-sm p-1 mb-1 rounded">
                <a className="navbar-brand" href="http://www.ase.ro">
                    AKADEMY 
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo01" >
                    <ul className="navbar-nav ml-auto">
                            <li className="nav-item">
                                <a className="nav-link" href="http://www.ase.ro">Link1</a>
                            </li>
                        </ul>
                </div>
                </nav>
                    <h2 className="text-center">Checkout your Quizzes!</h2>
                    <div className="list-group bg-dark shadow-sm p-1 mb-1 rounded" id="grup">
                        <ul className="list-group">
                            {this.state.tests.map(test => <li  className="list-group-item" key={test.testId}>{test.testId} {test.testName}</li>)}
                        </ul>
                    </div>
            </React.Fragment>
        )
    }

}