import React from 'react';
import axios from 'axios';
import './Login.css';
import {Register} from './../register/Register';
import {Quizzes} from './../quizz/Quizzes';

export class Login extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {};
        this.state.email = ""
        this.state.password = ""
        this.state.action = "login";
    }

    handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    }

    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    handleClickLogin = () => {
        axios.get('http://localhost:5000/users/login?email='+ this.state.email + '&password=' + this.state.password).then((res) => {
            if(res.data.length > 0){
                console.log(res.data)
                let user = res.data[0];
                if(user.userType === 2){
                    this.setState({action: "quizzes"})
                }
            }else{
                console.log(res.data.length)
            }
        }).catch((err) =>{
            console.log(err)
        })
    }

    handleClickRegister = () => {
        this.setState({
            action: "register"
        })
    }

    render(){
        if(this.state.action === "login"){
        return(
            <html lang="en" >
            <head>
              <title>Bootstrap Example</title>
              <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
              <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"></link>
              <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
              <link  rel='stylesheet' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic&amp;subset=latin,latin-ext'></link>
              <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'></link>
              <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons"></link>
              <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css"></link>
              
            </head>
        
            <body>
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
                <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
        
                <div className="modal-dialog text-center">
                    <div className="col-sm-8 main-section">
                        <div className="modal-content"styles={{marginTop:'70px'}}>
                            <form className="col-12">
                                <div className="form-group" >
                                    <input type="text" className="form-control" placeholder="Enter email" onChange={this.handleChangeEmail}></input>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" placeholder="Enter password" onChange={this.handleChangePassword}></input>
                                </div>
                                <button type="button" className="btn" onClick={this.handleClickLogin}><i className="fas fa-sign-in-alt" ></i>Login</button>
                            </form>
                            <div className="col-12 forgot">
                                <a href="#" onClick={this.handleClickRegister}>Do not have an account? Sign up now</a>
                            </div> 
                        </div>
                    </div>
                </div>
            </body>
            </html>
        );
        }else if(this.state.action === "register"){
            return(
                <Register></Register>
            );
        }else{
            return(
                <Quizzes></Quizzes>
            );
        }
    }
}