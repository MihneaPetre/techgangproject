const express = require('express');
const router = express.Router();
const pollConnection = require('../database').connection;

const bodyParser = require('body-parser')
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));


router.post("/",(req, res) => {
    var question=req.body;
    console.log(question);
    var sql=`insert into QUESTIONS(questionName, testId) values('${question.questionName}','${question.testId}')`;
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.status(400).send();
            throw err;
        }
        else
        {
            conn.query(sql, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status": err
                    });
                    throw err;
                }
                else{
                    res.json({
                        "code":200,
                        "status":"Insert succesfull !"
                    });
                   console.log("1 row inserted");
                }
                })
        }
        conn.release();
    })
});

router.get("/max", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            conn.query("SELECT * from QUESTIONS order by questionId desc limit 1", (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There are no question !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
        conn.release();
    })
})


router.get('/', (req, res) => {
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else
        {
            const questionName = req.query.questionName;
            const testId = req.query.testId;
            conn.query(`select * from QUESTIONS where questionName='${questionName}' and testId=${testId} `, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"Invalid question name or testId"
                    });
                }
                else
                    res.json(result);
            })
        }
        conn.release();
    })
})

router.get("/test/:id", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            const testId = req.params.id;
            conn.query("SELECT * from QUESTIONS where testId="+testId, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There are no question with that testId !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
        conn.release();
    })
})

module.exports = router;