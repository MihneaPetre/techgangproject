const express = require('express');
const router = express.Router();
const pollConnection = require('../database').connection;

const bodyParser = require('body-parser')
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));

router.get('/', (req, res) => {
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else
        {
            conn.query("select * from User", (err, result) =>{
                if(err)
                    throw err;
                else
                    res.json(result);
            })
        }
        conn.release();
    })
})

router.get('/login', (req, res) => {
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else
        {
            const email=req.query.email;
            const password= req.query.password;
            conn.query(`select * from User where emailAdress='${email}' and password='${password}' `, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"Invalid email or password"
                    });
                }
                else
                    res.json(result);
            })
        }
        conn.release();
    })
})

//insert an  user
router.post("/register",(req, res) => {
    var user=req.body;
    var data = user._registeredDate.day+"-"+user._registeredDate.month+"-"+user._registeredDate.year;
    console.log(user);
    console.log(data);
    var sql=`insert into USER(emailAdress, firstName, lastName, registerDate, userType, password, codeId)
    values('${user._emailAddress}','${user._firstName}','${user._lastName}', STR_TO_DATE('${data}', '%d-%m-%Y'),
            ${user._userType},'${user._password}',${user._codeId})`;
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.status(400).send();
            throw err;
        }
        else
        {
            conn.query(sql, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status": "Registration fail"
                    });
                    throw err;
                }
                else{
                    res.json({
                        "code":200,
                        "status":"Register succesfull !"
                    });
                   //res.status(200).send();
                   console.log("1 row inserted");
                }
                })
        }
    })
});

router.post("/register2",(req, res) => {
    const emailAddress=req.body.emailAdress;
    const firstName = req.body.firstName;
    const lastName = req.body.lastName;
    const userType = req.body.userType;
    const password = req.body.password;
    const codeId = req.body.codeId;
    
    var sql=`insert into USER(emailAdress, firstName, lastName, userType, password, codeId)
    values('${emailAddress}','${firstName}','${lastName}',${userType},'${password}',${codeId})`;
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            res.status(400).send();
            throw err;
        }
        else
        {
            conn.query(sql, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status": "Registration fail"
                    });
                    throw err;
                }
                else{
                    res.json({
                        "code":200,
                        "status":"Register succesfull !"
                    });
                   //res.status(200).send();
                   console.log("1 row inserted");
                }
                })
        }
    })
});

router.delete("/:id", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            const userId = req.params.id;
            conn.query("DELETE FROM User where userId="+userId, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There is no user with that id !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
    })
})

router.put("/update", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            const userId = req.body.userId;
            const password = req.body.password;
            conn.query(`update USER set password='${password}' where userId=${userId}`, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There is no user with that id !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
    })
})

module.exports = router;