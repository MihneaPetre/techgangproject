const express = require('express');
const router = express.Router();
const pollConnection = require('../database').connection;

const bodyParser = require('body-parser')
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));

router.get("/:id", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            const codeId = req.params.id;
            conn.query("SELECT codeId from CODE where isUsed=0 and codeId="+codeId, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"Your register code is not valid !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
    })
})

router.get("/max", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            conn.query("SELECT codeId, isUsed from CODE order by codeId desc limit 1", (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"Your register code is not valid !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
    })
})


module.exports = router;