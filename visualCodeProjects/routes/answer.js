const express = require('express');
const router = express.Router();
const pollConnection = require('../database').connection;

const bodyParser = require('body-parser')
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));


//insert an  answer
router.post("/",(req, res) => {
    var answer=req.body;
    console.log(answer);
    var sql=`insert into ANSWERS(answerName, isCorrect, questionId) 
             values('${answer.answerName}','${answer.isCorrect}','${answer.questionId}')`;
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.status(400).send();
            throw err;
        }
        else
        {
            conn.query(sql, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status": err
                    });
                    throw err;
                }
                else{
                    res.json({
                        "code":200,
                        "status":"Insert succesfull !"
                    });
                   console.log("1 row inserted");
                }
            })
        }
        conn.release();
    })
});

router.get("/question/:id", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            const questionId = req.params.id;
            conn.query("SELECT * from ANSWERS where questionId="+questionId, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There are no answer with that questionId !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
        conn.release();
    })
})


module.exports = router;