const express = require('express');
const router = express.Router();
const pollConnection = require('../database').connection;

const bodyParser = require('body-parser')
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended:true}));


//insert an  user
router.post("/",(req, res) => {
    var quizz=req.body;
    console.log(quizz);
    var sql=`insert into TESTS(testName, PIN) values('${quizz.testName}','${quizz.PIN}')`;
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.status(400).send();
            throw err;
        }
        else
        {
            conn.query(sql, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status": err
                    });
                    throw err;
                }
                else{
                    res.json({
                        "code":200,
                        "status":"Insert succesfull !"
                    });
                   console.log("1 row inserted");
                }
            })
        }
        conn.release();
    })
});

router.get("/max", (req, res) => {
    pollConnection.getConnection( (err,conn) => {
        if(err){
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else{
            conn.query("SELECT testId, testName, PIN from TESTS order by testId desc limit 1", (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"There are no quizz !"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
            })
        }
        conn.release();
    })
})

router.get("/",(req, res)=>{
    pollConnection.getConnection((err, conn)=>{
        if(err)
        {
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else
        {
            conn.query("select * from tests", (err, result) =>{
                if(err)
                    throw err;
                else
                    res.json(result);
            })
        }
        conn.release();
    })
})

router.get('/acces', (req, res) => {
    pollConnection.getConnection( (err, conn) => {
        if(err)
        {
            pollConnection.releaseConnection();
            res.json({
                "code":100,
                "status":"Error in connecting to database"
            });
            throw err;
        }
        else
        {
            const testName = req.query.testName;
            const PIN = req.query.PIN;
            conn.query(`select * from TESTS where testName='${testName}' and PIN=${PIN} `, (err, result) =>{
                if(err){
                    res.json({
                        "code":100,
                        "status":"Invalid question name or testId"
                    });
                }
                else{
                    console.log(result);
                    res.json(result);
                }
                    
            })
        }
        conn.release();
    })
})


module.exports = router;