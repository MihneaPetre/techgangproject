const mysql =require("mysql");

const dbConfig={
    host:"localhost",
    user:"root",
    password:"password",
    database:"quizzapp",
    port: 3000,
    insecureAuth:true,
};

const connection = mysql.createPool(dbConfig);

exports.connection = connection;
