create table USER( userId int auto_increment, emailAdress varchar(50),
firstName varchar(50), lastName varchar(50), registerDate datetime, userType int,
password varchar(50), codeId int, primary key (userId));

create table CODE(codeId int, validUntil datetime, isUsed bit, primary key (codeId));

create table SysUserType(sysUserTypeId int, userTypeName varchar(50), primary key 
(sysUserTypeId));

create table QUESTIONS( questionId int, questionName varchar(50), testId int, 
primary key (questionId));

create table TESTS( testId int, testName varchar(50), primary key (testId));
ALTER TABLE TESTS ADD COLUMN PIN VARCHAR(15) AFTER testName;
ALTER TABLE TESTS MODIFY COLUMN testId INT AUTO_INCREMENT

create table ANSWERS( answerId int, answerName varchar(50), isCorrect bit, questionId int,
primary key (answerId)); 
ALTER TABLE answers MODIFY COLUMN isCorrect boolean

create table RESULTS( resultId int, correctAnswered int, totalAnswers int, userId int, primary key (resultId));
ALTER TABLE RESULTS ADD COLUMN testId int AFTER userId;
alter table RESULTS add constraint FK_TEST2 foreign key (testId) 
references TESTS(testId);

alter table USER add constraint FK_CODE foreign key (codeID) 
references CODE(codeId);

alter table USER add constraint FK_USERTYPE foreign key (userType)
references SysUserType(sysUserTypeId);

alter table QUESTIONS add constraint FK_TEST foreign key (testId)
references TESTS(testId);
ALTER TABLE questions MODIFY COLUMN questionId INT AUTO_INCREMENT


alter table ANSWERS add constraint FK_QUESTION foreign key (questionId) 
references QUESTIONS(questionId);
ALTER TABLE answers MODIFY COLUMN answerId INT AUTO_INCREMENT

alter table RESULTS add constraint FK_USER foreign key (userId)
references USER(userId);
ALTER TABLE questions MODIFY COLUMN questionId INT AUTO_INCREMENT

insert into SysUserType values(1,'Profesor');
insert into SysUserType values(2,'Student');


insert into CODE values(12345, 1);
insert into CODE values(22345, 1);
insert into CODE values(11111, 0);

insert into USER values(1,'cosmin@gmail.com','Cirstescu','Robert', STR_TO_DATE('1-01-2018', '%d-%m-%Y'),
						1,'cosminIstetu',12345);
                        

insert into USER(emailAdress, firstName, lastName, registerDate, userType, password, codeId)
values('c@g.com','Circa','Adrian', STR_TO_DATE('1-01-2018', '%d-%m-%Y'),
						2,'123',22345);
                        
                        
                        select * from USER
                        select * from TESTS
                        select * from Questions
                        SELECT * FROM ANSWERS
                        insert into Questions(questionName, testId) values('fromBase',1);
                      
                        
                        SELECT codeId from CODE order by codeId desc limit 1
                        SELECT testId, testName, PIN from TESTS order by testId desc limit 1