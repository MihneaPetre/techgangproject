const express=require("express");
var app=express();


app.use(bodyParser.json());

var mysqlConnection= mysql.createConnection(dbConfig);
var mysqlConnectionTeachersStuff=mysql.createConnection(dbConfigTeachersStuff);
mysqlConnection.connect((err)=>{
    if(err)
    console.log("DB connection failed \n Error: "+ JSON.stringify(err));
    else
    console.log("DB connection succeded!");
})
mysqlConnectionTeachersStuff.connect((err)=>{
    if(err)
    console.log("DB connection failed \n Error: "+ JSON.stringify(err));
    else
    console.log("DB teachers stuff connection succeded!");
})

app.listen(3000,()=>{
    console.log("Express server is running at port no: 3000");
})

//get all students from the database
app.get("/students",(req,res)=>{
    mysqlConnection.query("SELECT * FROM students", (err, rows, fields)=>{
        if(!err)
        {console.log(rows);
        res.send(rows);
        }
        else
        console.log(err);
    });
});
//get all teachers questions from the database
app.get("/teachersquestions",(req,res)=>{
    mysqlConnectionTeachersStuff.query("SELECT * FROM teachersquestions", (err, rows, fields)=>{
        if(!err)
        {console.log(rows);
        res.send(rows);
        }
        else
        console.log(err);
    });
});
//get an specific student
app.get("/students/:id",(req,res)=>{
    mysqlConnection.query("SELECT * FROM students WHERE StudID= ?",[req.params.id], (err, rows, fields)=>{
        if(!err)
        {console.log(rows);
        res.send(rows);
        }
        else
        console.log(err);
    });
});
//delete an specific student
app.delete("/students/:id",(req,res)=>{
    mysqlConnection.query("DELETE FROM students WHERE StudID= ?",[req.params.id], (err, rows, fields)=>{
        if(!err)
        {console.log("Deleted successfully");
        res.send("Deleted successfully");
        }
        else
        console.log(err);
    });
});

//insert an  student
app.post("/students",(req,res)=>{
    var emp=req.body;
    var sql="SET @StudID=?; SET @Name=?; SET @Email=?; SET @Password=?; \
    CALL StudentsAddOrEdit(@StudID, @Name, @Email, @Password);";
    mysqlConnection.query(sql,[emp.StudID, emp.Name, emp.Email, emp.Password], (err, rows, fields)=>{
        if(!err)
        {console.log(rows);
        res.send(rows);
        }
        else
        console.log(err);
    })
});

//update an  student
app.put("/students",(req,res)=>{
    var emp=req.body;
    var sql="SET @StudID=?; SET @Name=?; SET @Email=?; SET @Password=?; \
    CALL StudentsAddOrEdit(@StudID, @Name, @Email, @Password);";
    mysqlConnection.query(sql,[emp.StudID, emp.Name, emp.Email, emp.Password], (err, rows, fields)=>{
        if(!err)
        {console.log("Updated successfully");
        res.send("Updated successfully");
        }
        else
        console.log(err);
    })
});