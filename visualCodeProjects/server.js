const express = require('express');
const app = express();
const bodyParser=require("body-parser");

const codeRoutes = require('./routes/code');
const userRoutes = require('./routes/user');
const quizzRoutes = require('./routes/quizz');
const questionRoutes = require('./routes/question');
const answersRoutes = require('./routes/answer');
app.use('/users',userRoutes);
app.use('/codes',codeRoutes);
app.use('/quizzes', quizzRoutes);
app.use('/questions', questionRoutes);
app.use('/answers',answersRoutes);

const PORT = 5000;
const server = app.listen(process.env.PORT || PORT, () => {
    const currentHost = server.address().address;
    const currentPort = server.address().port;
    console.log(`Server is running at http://${currentHost}:${currentPort}`);
})


module.exports = app;